import { create } from "zustand";

const useStore = create((set) => ({
  items: [],
  isLoading: false,
  error: null,
  hand: [],

  fetchItems: async () => {
    set({ isLoading: true, error: null });
    try {
      const response = await fetch("http://localhost:5151/cards");
      const data = await response.json();
      set({ items: data, isLoading: false });
    } catch (error) {
      set({
        items: [],
        isLoading: false,
        error: { message: "Pokemon Data Error" },
      });
    }
  },

  //Pick a card by its ID
  pickCard: (cardId) =>
    set((state) => ({
      items: state.items.map((card) =>
        card.id === cardId ? { ...card, isPicked: true } : card
      ),
      hand: [...state.hand, cardId],
    })),

  // Release a card by its ID
  releaseCard: (cardId) =>
    set((state) => ({
      items: state.items.map((card) =>
        card.id === cardId ? { ...card, isPicked: false } : card
      ),
      hand: state.hand.filter((id) => id !== cardId),
    })),
}));

export default useStore;

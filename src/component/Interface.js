"use client";
import React, { useEffect, useState } from "react";
import useStore from "../store";
import Modal from "./Modal/Modal";
import "./Interface.css";
import cute from "../cute.png";
import search from "../search.png";

const Interface = () => {
  const { items, hand, pickCard, releaseCard, isLoading, error, fetchItems } =
    useStore();

  useEffect(() => {
    fetchItems();
  }, [fetchItems]);

  const [searchQuery, setSearchQuery] = useState("");
  const [filterData, setFilterData] = useState(items);

  useEffect(() => {
    setFilterData(
      items.filter(
        (item) =>
          item.name.toLowerCase().includes(searchQuery.toLowerCase()) ||
          item.type.toLowerCase().includes(searchQuery.toLowerCase())
      )
    );
  }, [items, searchQuery]);

  const [modal, setModal] = useState(false);

  const releaseItemsDetails = items.filter((item) => hand.includes(item.id));

  const handleRelease = (itemId) => {
    releaseCard(itemId);
  };

  const handlePick = (itemId) => {
    pickCard(itemId);
  };

  const openModal = () => {
    setModal(!modal);
  };
  const closeModal = () => {
    setModal(false);
  };

  const calcStr = (item) => {
    return item.attacks ? item.attacks.length * 50 + "%" : "0%";
  };

  const calcWeak = (item) => {
    return item.attacks
      ? item.weaknesses.length * 100 > 100
        ? "100%"
        : item.weaknesses.length * 100 + "%"
      : "0%";
  };

  const calcHappiness = (item) => {
    const hp = parseInt(item.hp) || 0;
    const sumDamage = item.attacks
      ? item.attacks.reduce((accumulator, object) => {
          return accumulator + parseInt(checkSymbols(object.damage) || 0);
        }, 0)
      : 0;
    const sumData = (hp / 10 + (sumDamage / 10) * 2) / 5;
    return parseInt(sumData);
  };

  const checkSymbols = (str) => {
    const symbols = ["*", "+"];
    for (const symbol of symbols) {
      if (str.includes(symbol)) {
        return str.substring(0, str.length - 1);
      }
    }
    return str;
  };

  const COLORS = {
    Psychic: "#f8a5c2",
    Fighting: "#f0932b",
    Fairy: "#c44569",
    Normal: "#f6e58d",
    Grass: "#badc58",
    Metal: "#95afc0",
    Water: "#3dc1d3",
    Lightning: "#f9ca24",
    Darkness: "#574b90",
    Colorless: "#FFF",
    Fire: "#eb4d4b",
  };
  return (
    <>
      <div className="wrapper">
        <div className="touch-pad"></div>
        <div className="ribbon"></div>
        <div className="desktop">
          <div className="nav">
            <h1>My Pokedex</h1>
          </div>
          <div className="screen">
            {releaseItemsDetails.length > 0 && (
              <div className="pokedex-layout">
                {releaseItemsDetails.map((releaseItem) => (
                  <div className="pokedex-layout-block" key={releaseItem.id}>
                    <div className="card">
                      <div className="card-content">
                        <div className="card-image">
                          <img
                            src={releaseItem.imageUrl}
                            alt={releaseItem.name}
                            style={{ maxWidth: 200 }}
                          />
                        </div>
                        <div className="card-detail">
                          <div className="pokemon-header">
                            <h3>{releaseItem.name}</h3>
                            <p
                              style={{
                                backgroundColor: `${COLORS[releaseItem.type]}`,
                                paddingLeft: 5,
                                paddingRight: 5,
                              }}
                            >
                              {releaseItem.type}
                            </p>
                          </div>

                          <div className="pokemon-detail">
                            <div className="pokemon-detail-status">
                              <p>HP</p>
                            </div>
                            <div className="pokemon-detail-graph">
                              <div className="pokemon-progressbar">
                                <div
                                  className="pokemon-progress"
                                  style={{
                                    width: parseInt(releaseItem.hp)
                                      ? parseInt(releaseItem.hp) > 100
                                        ? "100%"
                                        : parseInt(releaseItem.hp) + "%"
                                      : "0%",
                                  }}
                                />
                              </div>
                            </div>
                          </div>
                          <div className="pokemon-detail">
                            <div className="pokemon-detail-status">
                              <p>STR</p>
                            </div>
                            <div className="pokemon-detail-graph">
                              <div className="pokemon-progressbar">
                                <div
                                  className="pokemon-progress"
                                  style={{ width: calcStr(releaseItem) }}
                                />
                              </div>
                            </div>
                          </div>
                          <div className="pokemon-detail">
                            <div className="pokemon-detail-status">
                              <p>WEAK</p>
                            </div>
                            <div className="pokemon-detail-graph">
                              <div className="pokemon-progressbar">
                                <div
                                  className="pokemon-progress"
                                  style={{ width: calcWeak(releaseItem) }}
                                />
                              </div>
                            </div>
                          </div>

                          <div className="pokemon-happiness">
                            {[...Array(calcHappiness(releaseItem)).keys()].map(
                              (item, index) => {
                                return (
                                  <img
                                    style={{
                                      width: 30,
                                      height: 30,
                                      marginRight: "3px",
                                    }}
                                    src={cute}
                                    key={index}
                                    alt="cute-icon"
                                  />
                                );
                              }
                            )}
                          </div>
                          <p
                            className="click-action"
                            onClick={() => handleRelease(releaseItem.id)}
                          >
                            X
                          </p>
                        </div>
                      </div>
                    </div>
                    {/* <button onClick={() => handleRelease(releaseItem.id)}>
                      Release
                    </button> */}
                  </div>
                ))}
              </div>
            )}
          </div>
          <div className="nav-footer">
            <div className="btn-add" onClick={openModal}>
              +
            </div>
          </div>
          <Modal show={modal} close={closeModal}>
            <div className="search-container">
              <div className="search-inner">
                <div className="search-icon">
                  <img
                    style={{ width: 32, height: 32 }}
                    src={search}
                    alt="search-icon"
                  />
                </div>
                <input
                  type="text"
                  placeholder="Find pokemon.."
                  value={searchQuery}
                  onChange={(e) => setSearchQuery(e.target.value)}
                />
              </div>
            </div>

            <div className="modal-body">
              {isLoading && <p>Loading data...</p>}
              {error && <p>Error: {error.message}</p>}
              {items.length > 0 && (
                <>
                  {filterData.map((pickItem) => (
                    <div key={pickItem.id}>
                      {!pickItem.isPicked ? (
                        <div className="card">
                          <div className="card-content">
                            <div className="card-image">
                              <img
                                src={pickItem.imageUrl}
                                alt={pickItem.name}
                                style={{ maxWidth: 200 }}
                              />
                            </div>
                            <div className="card-detail">
                              <div className="pokemon-header">
                                <h3>{pickItem.name}</h3>
                                <p
                                  style={{
                                    backgroundColor: `${COLORS[pickItem.type]}`,
                                    paddingLeft: 5,
                                    paddingRight: 5,
                                  }}
                                >
                                  {pickItem.type}
                                </p>
                              </div>

                              <div className="pokemon-detail">
                                <div className="pokemon-detail-status">
                                  <p>HP</p>
                                </div>
                                <div className="pokemon-detail-graph">
                                  <div className="pokemon-progressbar">
                                    <div
                                      className="pokemon-progress"
                                      style={{
                                        width: parseInt(pickItem.hp)
                                          ? parseInt(pickItem.hp) > 100
                                            ? "100%"
                                            : parseInt(pickItem.hp) + "%"
                                          : "0%",
                                      }}
                                    />
                                  </div>
                                </div>
                              </div>
                              <div className="pokemon-detail">
                                <div className="pokemon-detail-status">
                                  <p>STR</p>
                                </div>
                                <div className="pokemon-detail-graph">
                                  <div className="pokemon-progressbar">
                                    <div
                                      className="pokemon-progress"
                                      style={{ width: calcStr(pickItem) }}
                                    />
                                  </div>
                                </div>
                              </div>
                              <div className="pokemon-detail">
                                <div className="pokemon-detail-status">
                                  <p>WEAK</p>
                                </div>
                                <div className="pokemon-detail-graph">
                                  <div className="pokemon-progressbar">
                                    <div
                                      className="pokemon-progress"
                                      style={{ width: calcWeak(pickItem) }}
                                    />
                                  </div>
                                </div>
                              </div>

                              <div className="pokemon-happiness">
                                {[...Array(calcHappiness(pickItem)).keys()].map(
                                  (item, index) => {
                                    return (
                                      <img
                                        style={{
                                          width: 30,
                                          height: 30,
                                          marginRight: "3px",
                                        }}
                                        src={cute}
                                        key={index}
                                        alt="cute-icon"
                                      />
                                    );
                                  }
                                )}
                              </div>

                              <p
                                className="click-action"
                                onClick={() => handlePick(pickItem.id)}
                              >
                                ADD
                              </p>
                            </div>
                          </div>
                        </div>
                      ) : null}
                    </div>
                  ))}
                </>
              )}
            </div>
          </Modal>
        </div>
      </div>
    </>
  );
};

export default Interface;
